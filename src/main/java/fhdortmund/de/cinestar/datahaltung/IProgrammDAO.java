package fhdortmund.de.cinestar.datahaltung;

import java.io.IOException;
import java.util.List;

import fhdortmund.de.cinestar.filmverwaltung.Programm;



public interface IProgrammDAO {
	List<Programm> laden() throws IOException;
	void speichern(List<Programm> liste) throws IOException;
}