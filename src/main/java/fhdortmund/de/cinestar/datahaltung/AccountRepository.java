package fhdortmund.de.cinestar.datahaltung;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fhdortmund.de.cinestar.entity.Account;


@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
	
	 List<Account> findById(int id);

}

