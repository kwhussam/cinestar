package fhdortmund.de.cinestar.datahaltung;

import java.io.IOException;
import java.util.List;

import fhdortmund.de.cinestar.entity.Account;


public interface IAccountDAO {
	List<Account> laden() throws IOException;
	void speichern(List<Account> accountListe)  throws IOException;
}
