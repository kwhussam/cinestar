package fhdortmund.de.cinestar.filmverwaltung;

public class ProgrammBereitsVorhandenException extends Exception {

	public ProgrammBereitsVorhandenException() {
		super();
	}

	public ProgrammBereitsVorhandenException(String str) {
		super(str);
	}


}
