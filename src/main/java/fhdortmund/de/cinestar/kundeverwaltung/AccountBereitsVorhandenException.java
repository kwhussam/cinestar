package fhdortmund.de.cinestar.kundeverwaltung;

@SuppressWarnings("serial")
public class AccountBereitsVorhandenException extends Exception {

	public AccountBereitsVorhandenException() {
		super();
	}


	public AccountBereitsVorhandenException(String str) {
		super(str);
	}

}
