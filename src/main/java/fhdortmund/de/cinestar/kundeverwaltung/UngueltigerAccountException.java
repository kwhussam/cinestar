package fhdortmund.de.cinestar.kundeverwaltung;

public class UngueltigerAccountException extends Exception {
	public UngueltigerAccountException() {
		super();
	}
	
	public UngueltigerAccountException(String message) {
		super(message);
	}

}
