package fhdortmund.de.cinestar.entity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.sun.javafx.collections.MappingChange.Map;

import fhdortmund.de.cinestar.datahaltung.AccountRepository;

public class AccountController {
	
	 @Autowired
	    AccountRepository accountRespository;

	    @GetMapping("/blog")
	    public List<Account> index(){
	        return accountRespository.findAll();
	    }

}
