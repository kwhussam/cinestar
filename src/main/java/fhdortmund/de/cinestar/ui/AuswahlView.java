package fhdortmund.de.cinestar.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.TableRowSorter;

import fhdortmund.de.cinestar.filmverwaltung.Kategorie;
import fhdortmund.de.cinestar.filmverwaltung.Programm;



@SuppressWarnings("serial")
public class AuswahlView extends JInternalFrame {

	private MyTableModel tabellenModell;
	private JTable artikelTabelle;
	private TableRowSorter<MyTableModel> sorter;
	private JComboBox<Kategorie> kategorienAuswahl;

	
	private java.util.List<Programm> programmListe; 
	
	public AuswahlView(Window owner, Controller controller, 
			           java.util.List<Programm> programmliste) {
		super("Filmliste", false, 
				false, 
				false, 
				false);
		
		setSize(400, 300);
		setMinimumSize(new Dimension(400, 250));
		setLayout(new BorderLayout());
		
		// V6.0 
		this.programmListe = programmliste;

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton inWagen = new JButton("Book Ticket for this Film");
		buttonPanel.add(inWagen);
		add(buttonPanel, BorderLayout.SOUTH);

		JPanel panelCenter = new JPanel(new BorderLayout());
		panelCenter.setBorder(new LineBorder(Color.BLACK, 2));

		String[] spaltenUeberschriften = { "Kategorie", "Bezeichnung", "Preis" };
		
		tabellenModell = new MyTableModel(spaltenUeberschriften, 0);
		artikelTabelle = new JTable(tabellenModell);
		artikelTabelle.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		fuelleTabelleMitProgramme(programmliste);

		sorter = new TableRowSorter<MyTableModel>(tabellenModell);
		artikelTabelle.setRowSorter(sorter);
		sorter.setRowFilter(new KategorienFilter(Kategorie.Alle));

		JPanel kategorien = new JPanel();
		// Combobox zur Auswahl der Kategorien
		kategorienAuswahl = new JComboBox<Kategorie>();
		kategorienAuswahl.addItem(Kategorie.Alle);
		kategorienAuswahl.addItem(Kategorie.Action);
		kategorienAuswahl.addItem(Kategorie.Comedy);
		kategorienAuswahl.addItem(Kategorie.Drama);
		kategorienAuswahl.addItem(Kategorie.Horo);
		kategorien.add(new JLabel("Auswahl der Kategorie"));
		kategorien.add(kategorienAuswahl);

		
		panelCenter.add(new JScrollPane(artikelTabelle), BorderLayout.CENTER);
		panelCenter.add(kategorien, BorderLayout.SOUTH);

		add(panelCenter, BorderLayout.CENTER);

		/**************************************************************************/

		kategorienAuswahl.addActionListener(controller.new AuswahlListener()); // V6.0	
		inWagen.addActionListener(controller.new AuswahlListener());
		
		setVisible(true);
	}

	private void fuelleTabelleMitProgramme(java.util.List<Programm> programmliste) {
		Object[] zeile = new Object[3];
		for (Programm programm : programmliste) {
			zeile[0] = programm.getKategorie();
			zeile[1] = programm.getFilm().getFilmName();
			zeile[2] = programm.getPrise();
			tabellenModell.addRow(zeile);
		}
	}


	private int[] getListSelections() {
		int[] selected = artikelTabelle.getSelectedRows();

		for (int i = 0; i < selected.length; i++) {
			selected[i] = artikelTabelle.convertRowIndexToModel(selected[i]);
		}
		return selected;
	}

	
	private java.util.List<Programm> getProgrammListfromIndices(int[] listSelections) {
		ArrayList<Programm> selectedArticles = new ArrayList<Programm>();
		for (int i = 0; i < listSelections.length; i++)
			selectedArticles.add(programmListe.get(listSelections[i]));
		return selectedArticles;
	}
	
	
	
	List<Programm> getSelectedValuesList() {
		int[] selected = getListSelections();
		return getProgrammListfromIndices(selected);
	}

	void clearSelection() {
		artikelTabelle.clearSelection();
	}

	void tabellenAnsicht(Kategorie kategorie) {
		sorter.setRowFilter(new KategorienFilter(kategorie));
	}
}
