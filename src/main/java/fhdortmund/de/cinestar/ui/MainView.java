package fhdortmund.de.cinestar.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class MainView extends JFrame {

	private JButton anmelden;
	private JButton abmelden;
	private JButton zumEinkaufswagen;
	private JButton zurKasse;

	private JPanel buttonPanel; 

	public MainView(Controller controller)  {
		super("Willkommen Zum Cinestar Kino");
		
		setSize(600, 300);
		setMinimumSize(new Dimension(600, 300));
		
		fuelleButtonPanel();
		add(buttonPanel, BorderLayout.NORTH);

		// V5.0: Beobachter registrieren
		registriereListener(controller);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);  
	}

	// V6.0 Buttons jetzt zus�tzlich mit Icons. Icons zu finden 
	// in Verzeichnis images
	private void fuelleButtonPanel() {
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 4));
	
		anmelden = new JButton("anmelden", new ImageIcon("images/login.png"));
		buttonPanel.add(anmelden);

		abmelden = new JButton("abmelden", new ImageIcon("images/logout.png"));
		buttonPanel.add(abmelden);

		zumEinkaufswagen = new JButton("zumEinkaufswagen", 
				                       new ImageIcon("images/warenkorb.png"));
		buttonPanel.add(zumEinkaufswagen);

		zurKasse = new JButton("zurKasse");
		buttonPanel.add(zurKasse);

		iniButtons();
	}

	void iniButtons() {
		anmelden.setEnabled(true);
		abmelden.setEnabled(false);
		zumEinkaufswagen.setEnabled(false);
		zurKasse.setEnabled(false);
	}

	void enableButtons() {
		anmelden.setEnabled(false);
		abmelden.setEnabled(true);
		zumEinkaufswagen.setEnabled(true);
		zurKasse.setEnabled(true);
	}
	

	private void registriereListener(Controller controller) {
		Controller.MainViewListener hl = controller.new MainViewListener();
		anmelden.addActionListener(hl);
		abmelden.addActionListener(hl);
		zumEinkaufswagen.addActionListener(hl);
		zurKasse.addActionListener(hl);
	}
	
}