package fhdortmund.de.cinestar.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class AnmeldeView extends JDialog {

	private JTextField textfeldEmailAdresse;
	
	private JPasswordField textfeldPasswort; 
	
	JButton weiter = new JButton("weiter"); 

	public AnmeldeView(Window owner, Controller controller) {
		super(owner, "Anmelden");
		setModal(true);

		setSize(300, 170);
		setMinimumSize(new Dimension(300, 170)); 

		setLocationRelativeTo(owner); 

		setLayout(new GridLayout(5, 1));

		JLabel labelEmailAdresse = new JLabel("Geben Sie Ihre Emailadresse ein");
		JLabel labelPasswort = new JLabel("Geben Sie Ihr Passwort ein");
		textfeldEmailAdresse = new JTextField();
		textfeldPasswort = new JPasswordField(); // V6.0

		add(labelEmailAdresse);
		add(textfeldEmailAdresse);
		add(labelPasswort);
		add(textfeldPasswort);
		add(weiter);
		
		iniAnmeldeDialog();
		
    	Controller.AnmeldeListener al = controller.new AnmeldeListener();
		textfeldEmailAdresse.addKeyListener(al);
		textfeldPasswort.addKeyListener(al);
		weiter.addKeyListener(al);
		weiter.addActionListener(al);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		

	}

	public String getEmail() {
		return textfeldEmailAdresse.getText();
	}

	public String getPwd() {
		return String.valueOf(textfeldPasswort.getPassword());
	}

	private void iniAnmeldeDialog() {
		textfeldEmailAdresse.setText("");
		textfeldPasswort.setText("");
		textfeldEmailAdresse.requestFocus();
	}
	

	void focusToNextComponent() {
		Component c = this.getFocusOwner();
		if (c == textfeldEmailAdresse)
			textfeldPasswort.requestFocus();
		else
			if (c == textfeldPasswort)
				weiter.requestFocus();
	}
	

	boolean isWeiterButton(Object o) {
		return o == weiter;
	}	
}
