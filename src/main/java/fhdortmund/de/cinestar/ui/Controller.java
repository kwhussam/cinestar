package fhdortmund.de.cinestar.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.SortedMap;

import javax.swing.JComboBox;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fhdortmund.de.cinestar.entity.Account;
import fhdortmund.de.cinestar.fachlogik.Kino;
import fhdortmund.de.cinestar.filmverwaltung.Kategorie;
import fhdortmund.de.cinestar.filmverwaltung.Programm;
import fhdortmund.de.cinestar.filmverwaltung.ProgrammBereitsVorhandenException;
import fhdortmund.de.cinestar.kundeverwaltung.AccountBereitsVorhandenException;

import javax.swing.UIManager.LookAndFeelInfo;




public class Controller {

	private Kino kino;
	private MainView mainwindow; 
	

	private AuswahlView auswahlView;
	private AnmeldeView anmeldeView;
	private BestellView bestellView;
	private EinkaufswagenView ekwView;

	public Controller(Kino kino) {
		this.kino = kino;
	}


	public void start() {
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("Nimbus".equals(info.getName())) {
				try {
					UIManager.setLookAndFeel(info.getClassName());
				} catch (ClassNotFoundException | InstantiationException 
						| IllegalAccessException
						| UnsupportedLookAndFeelException e) {
				}
				break;
			}
		}
		
		mainwindow = new MainView(this);
		try {
			kino.laden();
			String text = "Willkommen bei " + kino.getBetreiber() + "!"; 
			new WillkommensDialog(mainwindow, text); // V6.0
		} catch (IOException | AccountBereitsVorhandenException 
				| ProgrammBereitsVorhandenException e) {
			new HinweisView(mainwindow, e.getMessage());
		}
	}


	private void ausfuehren(String befehl) {
		switch (befehl) {
		case "anmelden":
			if (anmelden()) {
				mainwindow.enableButtons();
				einkaufen();
			}
			break;
		case "abmelden":
			abmelden();
			mainwindow.iniButtons();
			break;
		case "zumEinkaufswagen":
			zumEinkaufswagen();
			break;
		case "zurKasse":
			zurKasse();
			break;
		}
	}

	private boolean anmelden() {
		anmeldeView = new AnmeldeView(mainwindow, this);
		anmeldeView.setVisible(true); 
		boolean anmeldenErfolgreich = false;
		try {
			String email = anmeldeView.getEmail();
			String pwd = anmeldeView.getPwd();
			Account account = new Account(email, pwd);
			if (kino.istGueltig(account)) {
				anmeldenErfolgreich = true;
				kino.anmelden(account);
			} else {
				new HinweisView(mainwindow, 
						   "Ung�ltige Anmeldeinformationen: Emailadresse = " 
	                       + email + ", Passwort = " + pwd);
			}
		} catch (Exception e) {
			new HinweisView(mainwindow, e.toString());
		}
		return anmeldenErfolgreich;
	}

	private void zumEinkaufswagen() {
		ekwView = new EinkaufswagenView(mainwindow, this);
		ekwView.setVisible(true); // V5.0 Verlagerung analog zu anmelden
	}

	
	private void einkaufen() {
		auswahlView = new AuswahlView(mainwindow, this, 
				                      kino.getProgrammliste());
		
		mainwindow.add(auswahlView);
	}
	private void zurKasse() {
		if (!kino.einkaufswagenIstLeer()) {
			bestellView = new BestellView(mainwindow, this, 
					        kino.getInhaltEinkaufswagen());
			bestellView.setVisible(true); // V5.0 Verlagerung analog zu anmelden

		} else
			new HinweisView(mainwindow, 
					        "Es sind keine Artikel in Ihrem Einkaufswagen!");
	}
	private void jetztKaufen() {
		if (!kino.einkaufswagenIstLeer()) {
			new VersandView(mainwindow, "Die Artikel werden umgehend versandt an " 
		                    + kino.getKundenName());
			// Einkaufswagen leeren
			kino.leereEinkaufswagen();
		
		}
	}


	private void abmelden() {
		auswahlView.dispose();
		mainwindow.remove(auswahlView);
		new HinweisView(mainwindow,
				"Auf Wiedersehen " + kino.getKundenName() + 
				"\nWir freuen uns auf Ihren n�chsten Besuch");
		kino.abmelden();
	}

	// Durchreichen an Webshop
	private void ausEinkaufswagenNehmen(Programm zuLoeschenderProgramm) {
		kino.ausEinkaufswagenNehmen(zuLoeschenderProgramm);
	}

	// Noch verwendet in EinkaufswagenView
	SortedMap<Programm, Integer> getInhaltEinkaufswagen() {
		return kino.getInhaltEinkaufswagen();
	}
	
	
	class AnmeldeListener extends KeyAdapter implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			anmeldeView.setVisible(false);
			anmeldeView.dispose();
		}

		public void keyPressed(KeyEvent evt) {
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
				if (anmeldeView.isWeiterButton(evt.getSource())) {
					anmeldeView.setVisible(false);
					anmeldeView.dispose();
				} else
					anmeldeView.focusToNextComponent();
			}
		}
	}


	class AuswahlListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			if (evt.getActionCommand().equals("comboBoxChanged")) { // V6.0 f�r
				// Kategorien-ComboBox
				JComboBox<Kategorie> cb = (JComboBox<Kategorie>) evt.getSource();
				Kategorie kategorie = (Kategorie) cb.getSelectedItem();
				auswahlView.tabellenAnsicht(kategorie);
			} else { // inEinkaufswagen
				List<Programm> aliste = auswahlView.getSelectedValuesList();
				kino.inEinkaufswagen(aliste);
				auswahlView.clearSelection();
			}

		}
	}

	// In Version mit mehreren Controllern KaufenBeobachter
	class BestellViewListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			jetztKaufen();
			bestellView.setVisible(false);
			bestellView.dispose();
		}
	}

	
	class EinkaufswagenListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			String befehl = evt.getActionCommand();
			if (befehl.equals("l�schen")) {
				

				Programm a = ekwView.changeInhalt();
				if (a != null) {
					ausEinkaufswagenNehmen(a);
				}
			} else { 
				ekwView.setVisible(false);
				ekwView.dispose();
				zurKasse();
			}
		}
	}
	
	class MainViewListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String befehl = e.getActionCommand();
			ausfuehren(befehl);

		}
	}

}
