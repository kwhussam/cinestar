package fhdortmund.de.cinestar.ui;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.WindowConstants;



	public class WillkommensDialog extends JDialog {
		public WillkommensDialog (JFrame owner, String text) {
			super(owner);
			setTitle(text);
			setModal(true);
			setSize(200, 150);
			setLocationRelativeTo(owner);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			

			
			setVisible(true);
		}
	}
