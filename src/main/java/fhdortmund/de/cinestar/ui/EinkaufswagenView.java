package fhdortmund.de.cinestar.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;




import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.border.LineBorder;
import javax.swing.table.TableRowSorter;

import fhdortmund.de.cinestar.filmverwaltung.Programm;
	@SuppressWarnings("serial")
	public class EinkaufswagenView extends JDialog {

		private JTable inhaltEinkaufswagen;
		private MyTableModel tabellenModell;

		private Controller controller;
		
		public EinkaufswagenView(Window owner, Controller controller) {
			super(owner, "Einkaufswagen");
			setModal(true);

			setSize(700, 200);
			setMinimumSize(new Dimension(700, 200));
			
			Point p = owner.getLocationOnScreen();
			setLocation(p.x + 100, p.y + 100);
			
			this.controller = controller; 

			JLabel labelInhaltEinkaufswagen = 
					new JLabel("Inhalt Ihres Einkaufswagens", JLabel.CENTER);
			
			/********************** V6.0 Tabelle statt Liste ***********************/
			String[] columns = new String[]{ "Kategorie", "ArtikelNr", "Bezeichnung", 
		                            "Preis", "Anzahl" };
			tabellenModell = new MyTableModel(columns, 0);
			inhaltEinkaufswagen = new JTable(tabellenModell);
			inhaltEinkaufswagen.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			inhaltEinkaufswagen.setEnabled(true);
			
			RowSorter<MyTableModel> sorter = 
					                 new TableRowSorter<MyTableModel>(tabellenModell);
			inhaltEinkaufswagen.setRowSorter(sorter);
			/*************************************************************************/
			
			JButton loeschen = createButton("l�schen");
			loeschen.setIcon(new ImageIcon("images/delete.png")); // V6.0; Icon -> Button
			
			JButton zurKasse = createButton("zurKasse");
			zurKasse.setBorder(new LineBorder(Color.ORANGE, 2, true));

			Box buttonPanel = new Box(BoxLayout.Y_AXIS);
			buttonPanel.add(loeschen);
			buttonPanel.add(Box.createVerticalGlue());
			buttonPanel.add(zurKasse);

			add(labelInhaltEinkaufswagen, BorderLayout.NORTH);
			add(buttonPanel, BorderLayout.EAST);
			
			add(new JScrollPane(inhaltEinkaufswagen), BorderLayout.CENTER);
			setInhalt(controller.getInhaltEinkaufswagen());
			
			loeschen.addActionListener(controller.new EinkaufswagenListener());
			zurKasse.addActionListener(controller.new EinkaufswagenListener());
			
			setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		}

		private JButton createButton(String text) {
			JButton b = new JButton(text);
			b.setMinimumSize(new Dimension(90, 30));
			b.setPreferredSize(new Dimension(90, 30));
			b.setMaximumSize(new Dimension(90, 30)); 
			return b;
		}
		
		
		private void setInhalt(SortedMap<Programm, Integer> inhalt) {
			Set<Entry<Programm, Integer>> es = inhalt.entrySet();
			Object[] zeile = new Object[5];
			for (Entry<Programm, Integer> e : es) {
				Programm a = e.getKey();
				int anzahl = e.getValue();
				zeile[0] = a.getKategorie();
				zeile[1] = a.getProgrammnummer();
				zeile[2] = a.getFilm().getFilmName();
				//zeile[3] = a.getPreis();// Where we have to put PRise
				zeile[4] = anzahl;
				tabellenModell.addRow(zeile);
			}
		}


		
		Programm changeInhalt() {
			Programm selectedArtikel = null;
			int selectedRow = inhaltEinkaufswagen.getSelectedRow();
			if (selectedRow >= 0) {
				int row = inhaltEinkaufswagen.convertRowIndexToModel(selectedRow);
				int artikelNr = (int) tabellenModell.getValueAt(row, 1);
				Set<Programm> programm = controller.getInhaltEinkaufswagen().keySet();
				for (Programm a : programm) {
					if (a.getProgrammnummer() == artikelNr) {
						selectedArtikel = a;
						break;
					}
				}
				int anzahl = (int) tabellenModell.getValueAt(row, 4);
				if (anzahl > 1)
					tabellenModell.setValueAt(anzahl - 1, row, 4);
				else {
					tabellenModell.removeRow(row);
				}
			}
			inhaltEinkaufswagen.clearSelection();
			return selectedArtikel;
		}


	}
