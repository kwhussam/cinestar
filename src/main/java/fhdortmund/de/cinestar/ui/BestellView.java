package fhdortmund.de.cinestar.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Window;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.Map.Entry;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

import fhdortmund.de.cinestar.filmverwaltung.Programm;


@SuppressWarnings("serial")
public class BestellView extends JDialog {

	private DefaultListModel<SubEntry<Programm, Integer>> listenModell;
	private JList<SubEntry<Programm, Integer>> anzeigeListe;

	public BestellView(Window owner, Controller controller, 
			SortedMap<Programm, Integer> artikelMap) {
		super(owner, "�bersicht �ber Ihre Besellung");
		setModal(true);
		setSize(700, 200);
		setMinimumSize(new Dimension(800, 200));

		Point p = owner.getLocationOnScreen();
		setLocation(p.x + 100, p.y + 100);

		JButton jetztKaufen = new JButton("Jetzt kaufen");
		add("East", jetztKaufen);

		listenModell = new DefaultListModel<SubEntry<Programm, Integer>>();
		anzeigeListe = new JList<SubEntry<Programm, Integer>>(listenModell);
		//Fontart gew�hrt "spaltenweise" Anzeige in Kombination mit toString-Methode
		// d
		anzeigeListe.setFont(new Font("Consolas",Font.PLAIN + Font.BOLD,14)); 
		
		anzeigeListe.setEnabled(false);
		add(new JScrollPane(anzeigeListe), BorderLayout.CENTER);
		fuelleArtikelInListe(artikelMap);

		double gPreis = berechneGesamtpreis(artikelMap);
		JLabel lb = new JLabel("Gesamtpreis: EUR " + gPreis);
		lb.setForeground(Color.red);
		add(lb, BorderLayout.SOUTH);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void fuelleArtikelInListe(SortedMap<Programm, Integer> artikelMap) {
		listenModell.removeAllElements();
		Set<Entry<Programm, Integer>> es = artikelMap.entrySet();
		for (Entry<Programm, Integer> e : es) {
			SubEntry<Programm, Integer> se = new SubEntry<Programm, 
					                             Integer>(e.getKey(), e.getValue());
			listenModell.addElement(se);
		}
	}

	private double berechneGesamtpreis(SortedMap<Programm, Integer> artikelMap) {
		double summe = 0;
		Set<Entry<Programm, Integer>> es = artikelMap.entrySet();
		for (Entry<Programm, Integer> e : es) {
			summe += e.getKey().getPrise() * e.getValue();
		}
		return summe;
	}
}
